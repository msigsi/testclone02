package com.qaagility.controller;

public class MathOperation {

    public int doDivision(int numerador, int dividendo) {
        if (dividendo == 0)
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return numerador / dividendo;
        }
    }

}
